'use strict';

describe('Service: Offer', function () {

  // load the service's module
  beforeEach(module('allegroAggregatorApp'));

  // instantiate service
  var Offer;
  beforeEach(inject(function (_Offer_) {
    Offer = _Offer_;
  }));

  it('should do something', function () {
    expect(!!Offer).toBe(true);
  });

});
