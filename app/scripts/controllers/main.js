'use strict';

angular.module('allegroAggregatorApp')
  .controller('MainCtrl', ['$scope', 'localStorageService', 'Offer', function ($scope, localStorageService, Offer) {
  	//values
    $scope.offers = [];
    $scope.offerType = "Aukcja"; //default auction type
    $scope.orderPredicate = "null";
  	//methods

    $scope.switchPredicate = function(pred) {
        if($scope.orderPredicate === pred) {
            $scope.orderPredicate = '-'+$scope.orderPredicate;
        }
        else {
            $scope.orderPredicate = pred;
        }
    }
    //to put the offer in the view
    $scope.saveOfferBind = function(key, offerData) {
        $scope.offers.push({
                'keyname': key,
                'data': offerData
            });
    };

  	$scope.init = function() {
        var keys = Object.keys(localStorage);
        //we're getting the localStorage of the app but we omit the first
        //record, which is the 'lastId'
        keys.splice(0, 1);
        keys.forEach(function (el, index) {
            //we remove the module 'ls.' part from key string
            el = el.substring(el.indexOf(".")+1, el.length);
            $scope.saveOfferBind(
                el,
                new Offer(localStorageService.get(el))
            );
        });

        var lastOffId = localStorageService.get('lastId');
        if(lastOffId) $scope.lastOffId = lastOffId;
        else $scope.lastOffId = -1;
    };
    $scope.saveOffer = function(title, price, type, duration) {
        $scope.lastOffId++;
    	var offerModel = {
            id: $scope.lastOffId,
    		title: title,
    		price: price,
    		link: (function() {
    			var words = title.split(" ");
    			var resultlink = "/offer/";
    			words.forEach(function(word, index) {
                    word = word.toLowerCase();
    				if(index != words.length-1)
    					resultlink += word + '-';
    				else
    					resultlink += word + '-' + $scope.lastOffId;
    			})
    			return resultlink;
    		})(),
    		type: type,
    		duration: duration
    	};
    	var offerDataObj = new Offer(offerModel);
        var offerNumber = $scope.offers.length;
    	offerDataObj.save(offerNumber, $scope.lastOffId);
        $scope.saveOfferBind(
            'offer' + offerNumber,
            offerDataObj
        );
        $scope.offerTitle = "";
        $scope.offerPrice = "";
        $scope.offerType = "Aukcja";
    };
    $scope.removeOffer = function(offer) {
        //remove offer from view
        $scope.offers.splice($scope.offers.indexOf(offer), 1);
        //remove offer from localstorage
        offer.data.delete(offer.keyname);
    };
    //main
    $scope.init(); //get all offers and initialize values
  }]);
