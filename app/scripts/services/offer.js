'use strict';

angular.module('allegroAggregatorApp')
  .factory('Offer', ['localStorageService', function (localStorageService) {
    function Offer(offerData) {
        if (offerData) {
            this.setData(offerData);
        }
        // Some other initializations related to offer
    };
    Offer.prototype = {
        setData: function(offerData) {
            angular.extend(this, offerData);
        },
        save: function(offNumber, lastId) {
            localStorageService.add('lastId', lastId);
            localStorageService.add('offer' + offNumber, this);
        },
        delete: function(offerKey) {
            localStorageService.remove(offerKey);
        }
    };
    return Offer;
  }]);
